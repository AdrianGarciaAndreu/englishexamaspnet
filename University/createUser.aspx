﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="createUser.aspx.cs" Inherits="University.createUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    Please, sign up for your new account:<br />
    <br />
    User name:<asp:TextBox ID="tbox_newUserName" runat="server"></asp:TextBox>
    <br />
    Password:<asp:TextBox ID="tbox_newUserPsswd" runat="server" TextMode="Password"></asp:TextBox>
    <br />
    <br />
    <br />
    Name:
    <asp:TextBox ID="txbox_name" runat="server"></asp:TextBox>
    <br />
    Surname:
    <asp:TextBox ID="txbox_surname" runat="server"></asp:TextBox>
    <br />
    Address:
    <asp:TextBox ID="txbox_address" runat="server"></asp:TextBox>
    <br />
    <br />
    Grade:
    <asp:DropDownList ID="dpdown_grade" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
    </asp:DropDownList>
    <br />
    Current grade year:
    <asp:TextBox ID="txbox_cuurentYear" runat="server" Width="95px"></asp:TextBox>
    <br />
    <br />
    Teacher
    <asp:CheckBox ID="cb_isTeacher" runat="server" />
    <br />
    <br />
    <asp:Button ID="btn_newUser_ok" runat="server" OnClick="btn_newUser_ok_Click" Text="OK" />
&nbsp;<asp:Label ID="lbl_msg" runat="server"></asp:Label>
&nbsp;
</asp:Content>
