﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using University.webservice;
using System.Web.Security;

namespace University
{

    public partial class login1 : System.Web.UI.Page
    {
        wsUV ws = new wsUV();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void OK_Click(object sender, EventArgs e)
        {
            String user = tbox_userName.Text;
            String psswd = tbox_psswd.Text;
            if (user.Length > 0 && psswd.Length > 0)
            {
                int _id = -1;
                _id = this.ws.LogIn(user, psswd);
                if (_id > 0)
                {
                    User u = this.ws.GetUserData(_id);

                    FormsAuthentication.SetAuthCookie(u.name, false);
                    Session["ID"] = u.ID;
                    Session["name"] = u.name;

                    if (u.isAdmin)
                    {
                        lbl_loginMSG.Text = u.name.ToString();
                        Response.Redirect("Admin/controlPage.aspx");
                    }
                    else
                    {
                        Response.Redirect("Users/Profile.aspx");
                    }

                }
                else { lbl_loginMSG.Text = "User or password were incorrect, please try again."; }


            }
            else
            {
                //Empty user Error
            }

        }



    }
}