﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using University.webservice;

namespace University.Admin
{
    public partial class deleteSubject : System.Web.UI.Page
    {
        int uID, sID;
        wsUV ws = new wsUV();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.uID = Convert.ToInt32(Request.QueryString["uID"]);
            this.sID = Convert.ToInt32(Request.QueryString["sID"]);

            delSubject(this.uID,this.sID);

        }


        protected void delSubject(int uID, int sID)
        {
            this.ws.DeleteFromSubject(uID, sID);
            Response.Redirect("Profile.aspx?uID="+this.uID);

        }


    }
}