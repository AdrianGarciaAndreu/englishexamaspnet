﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="addSubject.aspx.cs" Inherits="University.Admin.addSubject" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:DropDownList ID="dp_subjects" runat="server" OnSelectedIndexChanged="dp_subjects_SelectedIndexChanged">
    </asp:DropDownList>
&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btn_addSubject" runat="server" OnClick="btn_addSubject_Click" Text="Add" />
</asp:Content>
