﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="controlPage.aspx.cs" Inherits="University.Admin.controlPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lbl_controlPage" runat="server" Text="This is control page for admins"></asp:Label>
<br />
    <br />
    <asp:Button ID="btn_addUser" runat="server" PostBackUrl="~/Admin/Users/addUser.aspx" Text="Add" />
    <br />
    <br />
    <asp:Label ID="lbl_users_data" runat="server"></asp:Label>
    <br />
<br />
<asp:Button ID="btn_signOut" runat="server" OnClick="btn_signOut_Click" Text="Sign Out" />
</asp:Content>
