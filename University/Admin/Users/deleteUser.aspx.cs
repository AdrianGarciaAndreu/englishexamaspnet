﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using University.webservice;

namespace University.Admin.Users
{
    public partial class deleteUser : System.Web.UI.Page
    {
        wsUV ws = new wsUV();
        int uID;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.uID = Convert.ToInt32(Request.QueryString["uID"]);
            delUser(this.uID);
        }


        protected void delUser(int uID)
        {
            this.ws.DeleteUser(uID);
            Response.Redirect("/Admin/controlPage.aspx");
        }
    }
}