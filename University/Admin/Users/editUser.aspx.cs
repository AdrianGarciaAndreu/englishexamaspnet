﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using University.webservice;

namespace University.Admin.Users
{
    public partial class editUser : System.Web.UI.Page
    {
        wsUV ws = new wsUV();
        int uID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.uID = Convert.ToInt32(Request.QueryString["uID"]);
                populateGradesDropDown();
                populateFields(this.uID);
            }
        }


        protected void populateGradesDropDown()
        {
            Grade[] grades;
            grades = ws.GetGrades();
            if (grades.Length > 0)
            {

                dpdown_grade.DataSource = grades;
                dpdown_grade.DataTextField = "name";
                dpdown_grade.DataValueField = "ID";
                dpdown_grade.DataBind();
            }
            else
            {
                //Dropdown empty, no grades
                dpdown_grade.Enabled = false;
            }
        }



        protected void populateFields(int uID)
        {

            User u = this.ws.GetUserData(uID);
            txbox_name.Text = u.name;
            txbox_surname.Text = u.surname;
            txbox_address.Text = u.address;
            tbox_newUserName.Text = u.userID;


            Grade g = this.ws.GetUserGrade(uID); //User has grade or not
            if (g != null)
            {
                int i_selected = 0;
                int i = 0;
                foreach(ListItem item in dpdown_grade.Items)
                {
                    if(Convert.ToInt32(item.Value) == g.ID) { i_selected = i; break; }
                    i++;
                }

                dpdown_grade.SelectedIndex = i_selected;

            }

            txbox_cuurentYear.Text = u.currentYear.ToString();

            cb_isTeacher.Checked = u.isTeacher;


        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btn_newUser_ok_Click(object sender, EventArgs e)
        {

            User u = new User();
            bool changePass = false;
            u.ID = Convert.ToInt32(Request.QueryString["uID"]);
            u.name = txbox_name.Text.ToString();
            u.surname = txbox_surname.Text.ToString();
            u.address = txbox_address.Text.ToString();
            u.userID = tbox_newUserName.Text.ToString();

            string psswd = tbox_newUserPsswd.Text.ToString();
            u.pass = psswd;

            if (psswd.Length > 0) { changePass = true; }
            

            u.currentYear = Convert.ToInt32(txbox_cuurentYear.Text);
            u.ID_Grade = Convert.ToInt32(dpdown_grade.SelectedValue);
            u.isTeacher = Convert.ToBoolean(cb_isTeacher.Checked);
            u.isAdmin = false;


            bool succedd = this.ws.EditUser(u,changePass);
            lbl_msg.Text = succedd.ToString();
            if (succedd) { Response.Redirect("/Admin/controlPage.aspx"); }
            else { lbl_msg.Text = "User edit failed"; }



        }
    }
}