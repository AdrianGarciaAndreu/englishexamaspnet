﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using University.webservice;

namespace University.Admin
{
    public partial class controlPage : System.Web.UI.Page
    {
        wsUV ws = new wsUV();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Name"] == null || Session["ID"] == null)
            {
                Response.Redirect("/login.aspx");
            }
            loadProfileData();
        }

        protected void btn_signOut_Click(object sender, EventArgs e)
        {

            FormsAuthentication.SignOut();
            Session.Clear();
            Response.Redirect("/home.aspx"); 

        }



        protected void loadProfileData()
        {

            if (Session["ID"] != null)
            {
                int uID = Convert.ToInt32(Session["ID"]);
                //User u = this.ws.GetUserData(uID);
                User[] users = this.ws.GetStudents();

                if (users.Length > 0)
                {
                    lbl_users_data.Text = "<b>Students list </b> <br><br>";
                    foreach (User u in users)
                    {
                        lbl_users_data.Text += "<a href=\"Profile.aspx?uid="+u.ID+"\">"+u.surname + ", " + u.name + "</a>";
                        lbl_users_data.Text += " <a href=\"/Admin/Users/editUser.aspx?uID=" + u.ID + "\">Edit</a>  <a href=\"/Admin/Users/deleteUser.aspx?uID=" + u.ID + "\">Delete</a>";
                        lbl_users_data.Text += "<br>";

                    }
                }

            }

        }




    }
}