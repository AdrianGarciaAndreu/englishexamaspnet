﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="University.login1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    Please, log in<br />
<br />User name
            <asp:TextBox ID="tbox_userName" runat="server"></asp:TextBox>
<br />Password:
            <asp:TextBox ID="tbox_psswd" runat="server" TextMode="Password"></asp:TextBox>
<br />
<br />
<asp:Button ID="btn_ok" runat="server" OnClick="OK_Click" Text="OK" />
<br />
<br />
<asp:Label ID="lbl_loginMSG" runat="server"></asp:Label>
<br />
<br />
<br />
<br />
<asp:HyperLink ID="link_CreateUser" runat="server" NavigateUrl="~/CreateUser.aspx">[New User]</asp:HyperLink>
</asp:Content>
